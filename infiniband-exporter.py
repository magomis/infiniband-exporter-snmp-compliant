#!/usr/bin/env python3

import re
import time
import argparse
import subprocess
import os
import sys
import logging

from enum import Enum
from prometheus_client.core import CounterMetricFamily, GaugeMetricFamily
from prometheus_client import make_wsgi_app
from wsgiref.simple_server import make_server, WSGIRequestHandler

VERSION = "0.0.4"


class ParsingError(Exception):
    pass


class InfinibandItem(str, Enum):
    CA = 'ca'
    SWITCH = 'switch'


class InfinibandCollector(object):
    def __init__(self, can_reset_counter, node_name_map):
        self.can_reset_counter = can_reset_counter
        self.node_name_map = node_name_map

        self.node_name = {}
        if self.node_name_map:
            with open(self.node_name_map) as f:
                for line in f:
                    m = re.search(r'(?P<GUID>0x.*)\s+"(?P<name>.*)"', line)
                    if m:
                        self.node_name[m.group(1)] = m.group(2)

        self.scrape_with_errors = False
        self.metrics = {}

        self.maxed_out_count = 0

        self.checked_nodes_guid = set()

        # Description based on https://community.mellanox.com/s/article/understanding-mlx5-linux-counters-and-status-parameters # noqa: E501
        # and IB specification Release 1.3

        ## PERFORMANCE COUNTERS
        self.counter_info = {
            'PortXmitData': {
                'help': 'Total number of data octets transmitted on each VL, '
                        'between (and not including) the start of packet delimiter '
                        'and the VCRC (Variant Cyclic Redundancy Check); '
                        'including packets containing errors and excluding link packets. '
                        'Results reported as a multiple of 4 octets.',
                'SNMP': 'ifHCOutOctets',
                'severity': 'Informative',
                'bits': 64,
            },
            'PortRcvData': {
                'help': 'Total number of data octets received on each VL, '
                        'between (and not including) the start of packet delimiter '
                        'and the VCRC (Variant Cyclic Redundancy Check); '
                        'including packets containing errors and excluding link packets. '
                        'Results reported as a multiple of 4 octets.',
                'SNMP': 'ifHCInOctets',
                'severity': 'Informative',
                'bits': 64,
            },
            'PortUnicastXmitPkts': {
                'help': 'Total number of unicast packets transmitted on all '
                        'VLs, including unicast packets containing errors and excluding link packets.',
                'SNMP': 'ifHCOutUcastPkts',
                'severity': 'Informative',
                'bits': 64,
            },
            'PortUnicastRcvPkts': {
                'help': 'Total number of unicast packets received on all '
                        'VLs, including unicast packets containing errors and excluding link packets.',
                'SNMP': 'ifHCInUcastPkts',
                'severity': 'Informative',
                'bits': 64,
            },
            'PortMulticastXmitPkts': {
                'help': 'Total number of multicast packets transmitted on '
                        'all VLs, including multicast packets containing errors and excluding link packets.',
                'SNMP': 'ifHCOutMulticastPkts',
                'severity': 'Informative',
                'bits': 64,
            },
            'PortMulticastRcvPkts': {
                'help': 'Total number of multicast packets received on '
                        'all VLs, including multicast packets containing errors and excluding link packets.',
                'SNMP': 'ifHCInMulticastPkts',
                'severity': 'Informative',
                'bits': 64,
            },

            ## ERROR COUNTERS (1): XmitDiscards & details
            'PortXmitDiscards': {
                'help': 'Total number of outbound packets discarded by the '
                        'port because the port is down or congested.',
                'severity': 'Error',
                'SNMP': 'ifOutDiscards',
                'bits': 64,
            },
            'PortInactiveDiscards': {
                'help': 'Total number of packets discarded due to the port '
                        'being in the inactive state.',
                'severity': 'Error',
                'SNMP': 'ibIfInactiveDiscards',
                'bits': 16,
            },
            'PortNeighborMTUDiscards': {
                'help': 'Total outbound packets discarded by the port because '
                        'packet length exceeded the neighbor MTU.',
                'severity': 'Error',
                'SNMP': 'ibIfNeighborMTUDiscards',
                'bits': 16,
            },
            'PortSwLifetimeLimitDiscards': {
                'help': 'Total number of outbound packets discarded by the port'
                        'because the switch lifetime limit was exceeded. ',
                'severity': 'Error',
                'SNMP': 'ibIfSwLifetimeLimitDiscards',
                'bits': 16,
            },
            'PortSwHOQLifetimeLimitDiscards': {
                'help': 'Total number of outbound packets discarded by the port'
                        'because the HoQ (Head of Queue: length of time a packet can remain at the head of a VL queue)'
                        'lifetime limit of the switch containing the port was exceeded. ',
                'severity': 'Error',
                'SNMP': 'ibIfSwHOQLifetimeLimitDiscards',
                'bits': 16,
            },
            ## ERROR COUNTERS (2): RcvErrors & details (error counters)
            'PortRcvErrors': {
                'help': 'Total number of packets containing an error that '
                        'were received on the port.',
                'severity': 'Informative',
                'SNMP': 'ifInErrors',
                'bits': 64,
            },
            'PortLocalPhysicalErrors': {
                'help': 'Total number of packets received containing a physical error such as CRC '
                        '(Cyclic Redundancy Check) error -ICRC, VCRC, LPCRC- that cause entry into the BAD PACKET'
                        ' or BAD PACKET discard states of the packet receiver state machine.',
                'severity': 'Error',
                'SNMP': 'ibIfLocalPhysicalErrors',
                'bits': 16,
            },
            'PortMalformedPktErrors': {
                'help': 'Total number of packets received on the port containing malformed data or link packet errors.',
                'SNMP': 'ibIfMalformedPktErrors',
                'severity': 'Error',
                'bits': 16,
            },
            'PortBufferOverrunErrors': {
                'help': 'Total number of packets received on the part '
                        'discarded due to buffer overrrun.',
                'SNMP': 'ibIfBufferOverrunErrors',
                'severity': 'Error',
                'bits': 16,
            },
            'PortDLIDMappingErrors': {
                'help': 'Total number of packets on the port that could not '
                        'be forwarded by the switch due to "Destination LID" (DLID) mapping errors.',
                'SNMP': 'ibIfDLIDMappingErrors',
                'severity': 'Error',
                'bits': 16,
            },
            'PortVLMappingErrors': {
                'help': 'Total number of packets on the port that could not '
                        'be forwarded by the switch due to Virtual Lane (VL) mapping errors.',
                'SNMP': 'ibIfVLMappingErrors',
                'severity': 'Error',
                'bits': 16,
            },
            'PortLoopingErrors': {
                'help': 'Total number of packets on the port that could not be forwarded by the switch '
                        'due to looping errors (output port = input port).',
                'SNMP': 'ibIfLoopingErrors',
                'severity': 'Error',
                'bits': 16,
            },
            ## The rest of error counters following perfquery order (also: rdma-core/infiniband-diags/libibmad/src/fields.c L247-261)
            'SymbolErrorCounter': {
                'help': 'Total number of minor link errors detected on one '
                        'or more physical lanes.',
                'SNMP': 'ibIfSymbolErrors',
                'severity': 'Error',
                'bits': 64,
            },
            'LinkErrorRecoveryCounter': {
                'help': 'Total number of times the Port Training state machine has successfully completed'
                        'the link error recovery process.',
                'SNMP': 'ibIfLinkErrorRecovery',
                'severity': 'Error',
                'bits': 64,
            },
            'LinkDownedCounter': {
                'help': 'Total number of times the Port Training state machine has failed'
                        'the link error recovery process and downed the link.',
                'SNMP': 'ibIfLinkDowned',
                'severity': 'Error',
                'bits': 64,
            },
            'PortRcvRemotePhysicalErrors': {
                'help': 'Total number of packets received on the port which were marked with the EBP delimiter.',
                'SNMP': 'ibIfRcvRemotePhysicalErrors',
                'severity': 'Error',
                'bits': 64,
            },
            'PortXmitConstraintErrors': {
                'help': 'Total number of packets not transmitted from the switch physical port due to: '
                        '(1) FilterRawOutbound is true and packet is raw or (2)'
                        'PartitionEnforcementOutbound is true and packet fails partition key check or IP version check',
                'SNMP': 'ibIfOutConstraintErrors',
                'severity': 'Error',
                'bits': 64,
            },
            'PortRcvConstraintErrors': {
                'help': 'Total number of packets received on the switch physical port that are discarded due to:'
                        '(1) FilterRawInbound is true and packet is raw or (2)'
                        'PartitionEnforcementInbound is true and packet fails partition key check or IP version check',
                'SNMP': 'ibIfInConstraintErrors',
                'severity': 'Error',
                'bits': 64,
            },
            'LocalLinkIntegrityErrors': {
                'help': 'The number of times that the count of local physical errors' 
                        'exceeded the threshold specified by LocalPhyErrors.',
                'SNMP': 'ibIfLocalLinkIntegrityErrors',
                'severity': 'Error',
                'bits': 64,
            },
            'PortExcessiveBufferOverrunErrors': {
                'help': 'The number of times that OverrunErrors consecutive flow control update periods occurred, '
                        'each having at least one overrun error.',
                'SNMP': 'ibIfExcessiveBufferOverrunErrors',
                'severity': 'Error',
                'bits': 64,                          # To be checked and filled
            },
            'QP1Dropped': {
                'help': 'The number of GSI (General Services Interface) packets dropped. Unlike other Queue Pairs (QP),'
                        ' which are created and destroyed on an as-needed basis, QP1 is permanently implemented.'
                        'Unlike the subnet management packets transferred on VL15, GSI packets do not require high priority.',
                'SNMP': 'ibIfQP1Dropped',
                'severity': 'Error',
                'bits': 64,                         # To be checked and filled
            },
            'VL15Dropped': {
                'help': 'The number of incoming VL15 packets dropped due to resource '
                        'limitations (for example, lack of buffers) in the port.',
                'SNMP': 'ibIfVL15Dropped',
                'severity': 'Error',
                'bits': 64,
            },

            'PortXmitWait': {
                'help': 'The number of ticks during which the port had data '
                        'to transmit but no data was sent during the entire '
                        'tick (either because of insufficient credits or '
                        'because of lack of arbitration).',
                'SNMP': 'ibIfOutWait',
                'severity': 'Informative',
                'bits': 64,
            },
        }

        self.gauge_info = {
            'linkSpeedActive': {
                'help': 'Link current speed per lane.',
            },
            'linkWidthActive': {
                'help': 'Lanes per link.',
            }
        }

        self.additional_gauge_info = {
            'ifAdminStatus': {
                'help': 'Desired (logical) link state. 1: Active, 2: Down, 3: Initialized, 4: Armed, 5: Unknown',
            },
            'ifOperStatus': {
                'help': 'Current operational (physical) link state. 1: LinkUp, 2: Disabled, 3: Polling, 4: Sleep, 5: PortConfigurationTraining',
            }
        }

        self.dict_link_status = {
            'ifAdminStatus': {'Active': 1, 'Down': 2, 'Initializing': 3, 'Armed': 4, 'Unknown': 5
                              },
            'ifOperStatus': {'LinkUp': 1, 'Disabled': 2, 'Polling': 3, 'Sleep': 4, 'PortConfigurationTesting': 5
                             }
        }

        self.bad_status_error_metric_name = 'infiniband_bad_status_error'
        self.bad_status_error_metric_help = 'Bad status error catched from STDERR by ibqueryerrors.'
        self.bad_status_error_metric_labels = ['path', 'status', 'error']
        self.bad_status_error_pattern = r'src\/query\_smp\.c\:[\d]+\; (?:mad|umad) \((DR path .*) Attr .*\) bad status ([\d]+); (.*)'  # noqa: E501
        self.bad_status_error_prog = re.compile(self.bad_status_error_pattern)

        self.query_failed_error_metric_name = 'infiniband_query_failed_error'
        self.query_failed_error_metric_help = 'Failed query catched from STDERR by ibqueryerrors.'
        self.query_failed_error_metric_labels = ['counter_name', 'local_name', 'lid', 'port']
        self.query_failed_error_pattern = r'ibwarn: \[\d+\] query_and_dump: (\w+) query failed on (.*), Lid (\d+) port (\d+)'
        self.query_failed_error_prog = re.compile(self.query_failed_error_pattern)

        self.mad_rpc_recv_failed_pattern = r'ibwarn: \[\d+\] _do_madrpc: recv failed: [\w\s]+'
        self.mad_rpc_recv_failed_prog = re.compile(self.mad_rpc_recv_failed_pattern)

        self.mad_rpc_failed_error_metric_name = 'infiniband_mad_rpc_failed_error'
        self.mad_rpc_failed_error_metric_help = 'ibwarn_mad_rpc error catched from STDERR by ibqueryerrors.'
        self.mad_rpc_failed_error_metric_labels = ['portid']
        self.mad_rpc_failed_error_pattern = r'ibwarn: \[\d+\] mad_rpc: _do_madrpc failed; dport \(([\w;\s]+)\)'
        self.mad_rpc_failed_error_prog = re.compile(self.mad_rpc_failed_error_pattern)

        self.query_cap_mask_error_metric_name = 'infiniband_query_cap_mask_error'
        self.query_cap_mask_error_metric_help = 'ibwarn_query_cap_mask error catched from STDERR by ibqueryerrors.'
        self.query_cap_mask_error_metric_labels = ['counter_name', 'local_name', 'portid', 'port']
        self.query_cap_mask_error_pattern = r'ibwarn: \[\d+\] query_cap_mask: (\w+) query failed on (.*), ([\w;\s]+) port (\d+)'
        self.query_cap_mask_error_prog = re.compile(self.query_cap_mask_error_pattern)

        self.print_error_metric_name = 'infiniband_print_error'
        self.print_error_metric_help = 'ibwarn_print_error catched from STDERR by ibqueryerrors.'
        self.print_error_metric_labels = ['counter_name', 'local_name', 'portid', 'port']
        self.print_error_pattern = r'ibwarn: \[\d+\] print_errors: (\w+) query failed on (.*), ([\w;\s]+) port (\d+)'
        self.print_error_prog = re.compile(self.print_error_pattern)

        self.ibqueryerrors_header_regex_str = r'^Errors for (?:0[x][\da-f]+ )?\"(.*)\"$'
        self.ibqueryerrors_header_regex_str_only_counters = r'^Data Counters for (?:0[x][\da-f]+ )\"(.*)\"$'

        self.switch_all_ports_pattern = re.compile(r'\s*GUID 0[x][\da-f]+ port ALL: (?:\[.*\])+')

        self.port_pattern = re.compile(r'\s*GUID (0x.*) port (\d+):(.*)')
        self.link_pattern = re.compile(r'\s*Link info:\s+(\d+)\s+(\d+)\[\s+\] ==\(')  # LID - port (local)

        self.active_link_pattern = re.compile(
            r'\s*Link info:\s+(?P<LID>\d+)\s+(?P<port>\d+).*(?P<linkWidthActive>\d)X\s+(?P<linkSpeedActive>[\d+\.]*) Gbps.*(?P<ifAdminStatus>[A-Z][a-z]*)\/  (?P<ifOperStatus>[A-Za-z]*).*(?P<remote_GUID>0x\w+)\s+(?P<remote_LID>\d+)\s+(?P<remote_port>\d+).*\"(?P<node_name>.*)\"')
        self.down_link_pattern = re.compile(
            r'\s*Link info:\s+(?P<LID>\d+)\s+(?P<port>\d+).*\s(?P<ifAdminStatus>[A-Za-z]*)\/\s*(?P<ifOperStatus>[A-Za-z]*)')

    def chunks(self, x, n):  # Retrieve pairs: port counters data - link info
        for i in range(0, len(x), n):
            yield x[i:i + n]

    def parse_counter(self, s):
        counters = {}

        for counter in re.findall(r'\[(.*?)\]', s):
            c = re.search(r'(\w+) == (\d+).*?', counter)
            if c and c.group(1) in self.counter_info:
                counters[c.group(1)] = int(c.group(2))
        return counters

    def reset_counter(self, guid, port, reason):
        if guid in self.node_name:
            switch_name = self.node_name[guid]
        else:
            switch_name = guid

        if self.can_reset_counter:
            logging.info('Reseting counters on "{sw}" port {port} due to {r}'.format(  # noqa: E501
                sw=switch_name,
                port=port,
                r=reason
            ))
            process1 = subprocess.Popen(['./../rdma-core-lhcb-eb-daq/build/bin/perfquery', '-R', '-G', guid, port],
                                        stdout=subprocess.PIPE)
            process1.communicate()
            process2 = subprocess.Popen(['./../rdma-core-lhcb-eb-daq/build/bin/perfquery', '--xmtdisc', '-R', '-G', guid, port],
                                        stdout=subprocess.PIPE)
            process2.communicate()
            process3 = subprocess.Popen(['./../rdma-core-lhcb-eb-daq/build/bin/perfquery', '--rcverr', '-R', '-G', guid, port],
                                        stdout=subprocess.PIPE)
            process3.communicate()


        else:
            logging.warning('Counters on "{sw}" port {port} is maxed out on {r}'.format(  # noqa: E501
                sw=switch_name,
                port=port,
                r=reason
            ))
            self.maxed_out_count += 1

    def build_stderr_metrics(self, stderr):
        logging.debug('Processing stderr errors retrieved by ibqueryerrors')

        bad_status_error_metric = GaugeMetricFamily(
            self.bad_status_error_metric_name,
            self.bad_status_error_metric_help,
            labels=self.bad_status_error_metric_labels)

        query_failed_error_metric = GaugeMetricFamily(
            self.query_failed_error_metric_name,
            self.query_failed_error_metric_help,
            labels=self.query_failed_error_metric_labels)

        mad_rpc_failed_error_metric = GaugeMetricFamily(
            self.mad_rpc_failed_error_metric_name,
            self.mad_rpc_failed_error_metric_help,
            labels=self.mad_rpc_failed_error_metric_labels)

        query_cap_mask_error_metric = GaugeMetricFamily(
            self.query_cap_mask_error_metric_name,
            self.query_cap_mask_error_metric_help,
            labels=self.query_cap_mask_error_metric_labels)

        print_error_metric = GaugeMetricFamily(
            self.print_error_metric_name,
            self.print_error_metric_help,
            labels=self.print_error_metric_labels)

        stderr_metrics = [
            bad_status_error_metric,
            query_failed_error_metric,
            mad_rpc_failed_error_metric,
            query_cap_mask_error_metric,
            print_error_metric]

        error = False

        for line in stderr.splitlines():
            logging.debug('STDERR line: {}'.format(line))

            if self.process_bad_status_error(line, bad_status_error_metric):
                pass
            elif self.process_query_failed_error(line, query_failed_error_metric):
                pass
            elif self.mad_rpc_recv_failed_prog.match(line):
                pass
            elif self.process_mad_rpc_failed(line, mad_rpc_failed_error_metric):
                pass
            elif self.process_query_cap_mask(line, query_cap_mask_error_metric):
                pass
            elif self.process_print_errors(line, print_error_metric):
                pass
            else:
                if not error:
                    error = True
                logging.error('Could not process line from STDERR: {}'.format(line))

        return stderr_metrics, error

    def process_bad_status_error(self, line, error):

        result = self.bad_status_error_prog.match(line)

        if result:
            labels = [
                result.group(1),  # path
                result.group(2),  # status
                result.group(3)]  # error

            error.add_metric(labels, 1)

            return True

        return False

    def process_query_failed_error(self, line, error):

        result = self.query_failed_error_prog.match(line)

        if result:
            labels = [
                result.group(1),  # counter_name
                result.group(2),  # local_name
                result.group(3),  # lid
                result.group(4)]  # port

            error.add_metric(labels, 1)

            return True

        return False

    def process_mad_rpc_failed(self, line, error):

        result = self.mad_rpc_failed_error_prog.match(line)

        if result:
            labels = [result.group(1)]  # portid

            error.add_metric(labels, 1)

            return True

        return False

    def process_query_cap_mask(self, line, error):

        result = self.query_cap_mask_error_prog.match(line)

        if result:
            labels = [
                result.group(1),  # counter_name
                result.group(2),  # local_name
                result.group(3),  # portid
                result.group(4)]  # port

            error.add_metric(labels, 1)

            return True

        return False

    def process_print_errors(self, line, error):

        result = self.print_error_prog.match(line)

        if result:
            labels = [
                result.group(1),  # counter_name
                result.group(2),  # local_name
                result.group(3),  # portid
                result.group(4)]  # port

            error.add_metric(labels, 1)

            return True

        return False

    def init_metrics(self):

        for gauge_name in self.gauge_info:
            self.metrics[gauge_name] = GaugeMetricFamily(
                gauge_name,
                self.gauge_info[gauge_name]['help'],
                labels=[
                    'ifAlias',  # local GUID
                    'ifDescr',  # port number if switch, port name if server
                    'ifName',  # local LID
                    'ifIndex',  # local port number
                    'ifAliasRemote',
                    'ifDescrRemote',
                    'ifNameRemote',
                    'ifIndexRemote',
                ])

        for gauge_name in self.additional_gauge_info:
            self.metrics[gauge_name] = GaugeMetricFamily(
                gauge_name,
                self.additional_gauge_info[gauge_name]['help'],
                labels=[
                    'ifAlias',  # local GUID
                    'ifDescr',  # port number if switch, port name if server
                    'ifName',  # local LID
                    'ifIndex',  # local port number
                    'ifAliasRemote',
                    'ifDescrRemote',
                    'ifNameRemote',
                    'ifIndexRemote',
                ])

        for counter_name in self.counter_info:
            metric_name = self.counter_info[counter_name]['SNMP']
            self.metrics[metric_name] = CounterMetricFamily(
                metric_name,
                self.counter_info[counter_name]['help'],
                labels=[
                    'ifAlias',  # local GUID
                    'ifDescr',  # port number if switch, port name if server
                    'ifName',  # local LID
                    'ifIndex',  # local port number
                    'ifAliasRemote',
                    'ifDescrRemote',
                    'ifNameRemote',
                    'ifIndexRemote',
                ])

    def process_item(self, component, item, only_counters_flag=False):  # OK
        """
        The method processes ibquery ca and switch data.
        Parameters:
            * component (InfinibandItem)
            * item (Generator[List[str]])
        Throws:
            ParsingError - Raised during parsing of input content due to inconsistencies.
            RuntimeError - Raised on wrong data type for parameter passed.
        """

        if not isinstance(component, InfinibandItem):
            raise RuntimeError('Wrong data type passed for component: {}'.format(type(component)))

        if not isinstance(item, list):
            raise RuntimeError('Wrong data type passed for item: {}'.format(type(item)))

        if len(item) != 2:
            raise ParsingError('Item data incomplete:\n{}'.format(item[0]))

        name = item[0]  # Node name: from node-name-map for switches, 2 words for HCA
        data = item[1]  # Counters data + Link info (all ports one after the other for switches)

        item_lines = data.lstrip().splitlines()  # separate data counters - link info (& aggregated counters for switches)

        if InfinibandItem.SWITCH == component and only_counters_flag == False:

            switch_all_ports = item_lines[0]  # Aggregated counters data
            match_switch_all_ports = self.switch_all_ports_pattern.fullmatch(switch_all_ports)

            if match_switch_all_ports:
                del item_lines[0]  # Drop aggregated counters data (we want individual port counters of each node)

        for item_pair in self.chunks(item_lines, 2):

            if len(item_pair) == 2:

                port_item, link_item = item_pair  # strings: port counters & link info

                match_port = self.port_pattern.match(port_item)  # GUID - port num. - counters

                if match_port:
                    port = int(match_port.group(2))  # Retrieve port number from the port counters string

                    if port > 0 and port < 41:
                        match_link = self.link_pattern.match(link_item)  # Link info (only beginning: local info)

                        if not match_link:
                            raise ParsingError('No link info line match for port:\n{}'.format(port_item))

                        m_active_link = self.active_link_pattern.match(link_item)

                        m_down_link = self.down_link_pattern.match(link_item)

                        if m_active_link:
                            # {'LID', 'port', 'Width', 'Speed', ****, 'remote_GUID', 'remote_LID', 'remote_port', 'node_name' (remote)}
                            # values: str
                            self.parse_item(component, name, match_port, m_active_link,
                                            only_counters_flag)

                        elif m_down_link:
                            self.parse_item(component, name, match_port, m_down_link, only_counters_flag, down_flag=True)

                        else:  # Implement other possible Admin- and OperStatus combinations to be implemented
                            pass

                elif port_item == '' or "##" in port_item:

                    if not (link_item == '' or "##" in link_item):
                        raise ParsingError('Inconsistent data found:\nitem_pair[0]: {}\nitem_pair[1]: {}'.
                                           format(item_pair[0], item_pair[1]))

                    continue
                else:
                    raise ParsingError('Inconsistent data found:\nitem_pair[0]: {}\nitem_pair[1]: {}'.
                                       format(item_pair[0], item_pair[1]))

            else:
                if not '##' in item_pair[0]:
                    raise ParsingError('Inconsistent data found:\n{}'.format(item_pair[0]))

    def parse_item(self, component, name, match_port, match_link,
                   only_counters_flag, down_flag=False):  # OK

        guid = match_port.group(1)
        port = match_port.group(2)
        # Get dictionary with counters' names already translated to SNMP as keys, and counters' readings as values:
        counters = self.parse_counter(
            match_port.group(3))  # Counters data [CounterName1 == Value1] [CounterName2 == Value2]

        if only_counters_flag:
            for gauge in self.gauge_info:
                if down_flag:
                    label_values = [
                        guid,
                        name,
                        match_link.group('LID'),
                        port,]
                    self.metrics[gauge].add_metric(label_values, 0)  # No speed, no bandwidth because the link is down
                else:
                    label_values = [
                        guid,
                        name,
                        match_link.group('LID'),
                        port,
                        match_link.group('remote_GUID'),
                        match_link.group('node_name'),
                        match_link.group('remote_LID'),
                        match_link.group('remote_port')]

                    self.metrics[gauge].add_metric(label_values, match_link.group(gauge))

            for gauge in self.additional_gauge_info:
                if down_flag:
                    label_values = [
                        guid,
                        name,  # Not convenient for aggregation nodes
                        match_link.group('LID'),
                        port,]
                else:
                    label_values = [
                        guid,
                        name,
                        match_link.group('LID'),
                        port,
                        match_link.group('remote_GUID'),
                        match_link.group('node_name'),
                        match_link.group('remote_LID'),
                        match_link.group('remote_port')]

                self.metrics[gauge].add_metric(label_values, self.dict_link_status[gauge][match_link.group(gauge)])

        for counter in counters:
            if down_flag:
                label_values = [
                    guid,
                    name,  # It's not convenient for aggregation nodes
                    match_link.group('LID'),
                    port,]
            else:
                label_values = [
                    guid,
                    name,
                    match_link.group('LID'),
                    port,
                    match_link.group('remote_GUID'),
                    match_link.group('node_name'),
                    match_link.group('remote_LID'),
                    match_link.group('remote_port')
                ]
            try:
                metric_name = self.counter_info[counter]['SNMP']

                self.metrics[metric_name].add_metric(label_values, counters[counter])

                if counters[counter] >= 2 ** (self.counter_info[counter]['bits'] - 1):  # noqa: E501
                    self.reset_counter(guid, port, counter)
            except KeyError:
                self.scrape_with_errors = True
                logging.error('Missing description for counter metric: {}'.format(counter))

        self.checked_nodes_guid.add(guid)

    def collect(self):

        logging.debug('Start of collection cycle')

        self.scrape_with_errors = False

        ibqueryerrors_duration = GaugeMetricFamily(
            'infiniband_ibqueryerrors_duration_seconds',
            'Number of seconds taken to run ibqueryerrors (both performance and errors data).')
        ibqueryerrors_duration_only_counters = GaugeMetricFamily(
            'infiniband_ibqueryerrors_only_counters_duration_seconds',
            'Number of seconds taken to run ibqueryerrors --counters (only performance data).')
        scrape_duration = GaugeMetricFamily(
            'infiniband_scrape_duration_seconds',
            'Number of seconds taken to collect and parse the stats.')
        scrape_start = time.time()
        scrape_ok = GaugeMetricFamily(
            'infiniband_scrape_ok',
            'Indicates with a 1 if the scrape was successful and complete, '
            'otherwise 0 on any non critical errors detected '
            'e.g. ignored lines from ibqueryerrors STDERR or parsing errors.')

        self.init_metrics()

        ibqueryerrors_args = [
            './../rdma-core-lhcb-eb-daq/build/bin/ibqueryerrors',
            '--verbose',
            '--details',                # Both XmitDisc and RcvErrors details (ibqueryerrors -h not concise)
            '--suppress-common',        # Suppress PortRcvSwitchRelayErrors because out-of-date: included in RcvErrors
            #                '--data',
            '--report-port',  # Port link info
            '--switch',
            '--ca']
        ibqueryerrors_args_only_counters = [
            './../rdma-core-lhcb-eb-daq/build/bin/ibqueryerrors',
            '--report-port',  # Port link info
            '--counters',
            '--verbose']
        if self.node_name_map:
            for x in [ibqueryerrors_args, ibqueryerrors_args_only_counters]:
                x.append('--node-name-map')
                x.append(self.node_name_map)

        ibqueryerrors_start = time.time()
        process = subprocess.Popen(ibqueryerrors_args,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        process_stdout, process_stderr = process.communicate()
        ibqueryerrors_stdout = process_stdout.decode("utf-8")

        if process_stderr:
            ibqueryerrors_stderr = process_stderr.decode("utf-8")

            logging.debug("STDERR output retrieved from ibqueryerrrors:\n%s",
                          ibqueryerrors_stderr)

            stderr_metrics, error = self.build_stderr_metrics(
                ibqueryerrors_stderr)

            for stderr_metric in stderr_metrics:
                yield stderr_metric

            if error:
                self.scrape_with_errors = True

        ibqueryerrors_duration.add_metric(
            [],
            time.time() - ibqueryerrors_start)
        yield ibqueryerrors_duration

        ibqueryerrors_start_only_counters = time.time()
        process_only_counters = subprocess.Popen(ibqueryerrors_args_only_counters,
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE)
        process_stdout_only_counters, process_stderr_only_counters = process_only_counters.communicate()
        ibqueryerrors_stdout_only_counters = process_stdout_only_counters.decode("utf-8")
        if process_stderr_only_counters:
            ibqueryerrors_stderr_only_counters = process_stderr_only_counters.decode("utf-8")

            logging.debug("STDERR output retrieved from ibqueryerrrors (only counters):\n%s",
                          ibqueryerrors_stderr_only_counters)

            stderr_metrics_only_counters, error_only_counters = self.build_stderr_metrics(
                ibqueryerrors_stderr_only_counters)

            for stderr_metric in stderr_metrics_only_counters:
                yield stderr_metric

            if error_only_counters:
                self.scrape_with_errors = True

        ibqueryerrors_duration_only_counters.add_metric(
            [],
            time.time() - ibqueryerrors_start_only_counters)
        yield ibqueryerrors_duration_only_counters

        content = re.split(self.ibqueryerrors_header_regex_str,
                           ibqueryerrors_stdout,
                           flags=re.MULTILINE)
        content_only_counters = re.split(self.ibqueryerrors_header_regex_str_only_counters,
                                         ibqueryerrors_stdout_only_counters,
                                         flags=re.MULTILINE)

        try:
            if not content:
                raise ParsingError('Input content is empty.')
            if not content_only_counters:
                raise ParsingError('***Input content (only counters) is empty.***')

            if not isinstance(content, list):
                raise RuntimeError('Input content should be a list.')
            if not isinstance(content_only_counters, list):
                raise RuntimeError('***Input content (only counters) should be a list.***')

            # Drop first line that is empty on successful regex split():
            for x in [content, content_only_counters]:
                if x[0] == '':
                    del x[0]
                else:
                    raise ParsingError('Inconsistent input content detected:\n{}'.format(x[0]))

            input_data_chunks = self.chunks(content, 2)  # counters info - link info (?)
            input_data_chunks_only_counters = self.chunks(content_only_counters, 2)
            for data_chunk in input_data_chunks_only_counters:
                if "SW-" in data_chunk[0]:
                    self.process_item(InfinibandItem.SWITCH, data_chunk, only_counters_flag=True)

                elif data_chunk[0] != "Mellanox Technologies Aggregation Node":  # Discard the CA communicating with virtual ports 41 of switches
                    self.process_item(InfinibandItem.CA, data_chunk, only_counters_flag=True)

                else:
                    pass

            for data_chunk in input_data_chunks:

                match_switch = self.switch_all_ports_pattern.match(data_chunk[1])

                if match_switch:
                    self.process_item(InfinibandItem.SWITCH, data_chunk)

                elif data_chunk[
                    0] != "Mellanox Technologies Aggregation Node":  # Discard the CA communicating with virtual ports 41 of switches
                    self.process_item(InfinibandItem.CA, data_chunk)
                else:
                    pass

            for counter_name in self.counter_info:
                metric_name = self.counter_info[counter_name]['SNMP']
                yield self.metrics[metric_name]

            for gauge_name in self.gauge_info:
                yield self.metrics[gauge_name]

            for gauge_name in self.additional_gauge_info:
                yield self.metrics[gauge_name]

        except ParsingError as e:
            logging.error(e)
            self.scrape_with_errors = True

        scrape_duration.add_metric([], time.time() - scrape_start)
        yield scrape_duration

        if self.scrape_with_errors:
            scrape_ok.add_metric([], 0)
        else:
            scrape_ok.add_metric([], 1)
        yield scrape_ok

        logging.debug('End of collection cycle')

        #print('\nNumber of maxed out ports due to PortXmitWait:\t', self.maxed_out_count)


# stolen from stackoverflow (http://stackoverflow.com/a/377028)
def which(program):
    """
    Python implementation of the which command
    """

    def is_exe(fpath):
        """ helper """
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, _ = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        paths = os.getenv("PATH", "/usr/bin:/usr/sbin:/sbin:/bin")

        for path in paths.split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


class NoLoggingWSGIRequestHandler(WSGIRequestHandler):
    def log_message(self, format, *args):
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Prometheus collector for a infiniband fabric')
    parser.add_argument(
        '--port',
        type=int,
        default=9683,
        help='Collector http port, default is 9683')
    parser.add_argument(
        '--can-reset-counter',
        default=False,
        dest='can_reset_counter',
        help='Will reset counter as required when maxed out. Can also be \
set with env variable CAN_RESET_COUNTER',
        action='store_true')
    parser.add_argument(
        '--node-name-map',
        action='store',
        default="/admin/etc/ib-node-name-map",
        dest='node_name_map',
        help='Node name map used by ibqueryerrors. Can also be set with env \
var NODE_NAME_MAP')
    parser.add_argument("--verbose", help="increase output verbosity", default=True,
                        action="store_true")
    parser.add_argument('-v',
                        '--version',
                        dest='print_version',
                        required=False,
                        action='store_true',
                        help='Print version number')

    args = parser.parse_args()

    if args.print_version:
        print(f"Version {VERSION}")
        sys.exit()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s - %(levelname)s - %(message)s')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s')

    if not which("ibqueryerrors"):
        logging.critical('Cannot find an executable ibqueryerrors binary in PATH')  # noqa: E501
        sys.exit(1)

    if args.node_name_map:
        logging.debug('Using node-name-map provided in args: {}'.format(
            args.node_name_map))
        node_name_map = args.node_name_map
    elif 'NODE_NAME_MAP' in os.environ:
        logging.debug('Using NODE_NAME_MAP provided in env vars: {}'.format(
            os.environ['NODE_NAME_MAP']))
        node_name_map = os.environ['NODE_NAME_MAP']
    else:
        logging.debug('No node-name-map was provided')
        node_name_map = None

    if args.can_reset_counter:
        logging.debug('can_reset_counter provided in args')
        can_reset_counter = True
    elif 'CAN_RESET_COUNTER' in os.environ:
        logging.debug('CAN_RESET_COUNTER provided in env vars')
        can_reset_counter = True
    else:
        logging.debug('Counters will not reset automatically')
        can_reset_counter = False

    app = make_wsgi_app(InfinibandCollector(
        can_reset_counter,
        node_name_map))
    httpd = make_server('', args.port, app,
                        handler_class=NoLoggingWSGIRequestHandler)
    httpd.serve_forever()
