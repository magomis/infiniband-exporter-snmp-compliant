# Infiniband-Exporter
Prometheus exporter for a Infiniband fabric. This exporter only needs to be installed on one server connected to the fabric, it will collect all the ports statistics on all the switches.

Metrics are identified by GUID, name, port number and LID for both local and remote port of the link. Each metric represents either a connection between 2 switches (internal connections) or a connection between a switch and a card in a server (external connections). 

The file /admin/etc/ib-node-name-map provides by default human friendly switches names for the LHCb DAQ network. This allows the quick identification of spine and leaf switches.

## Grafana dashboard examples:
[General](https://grafana.com/grafana/dashboards/13260)  
[Dashboard specifically built for the project: leaf switch performance of the LHCb DAQ network](https://lbgrafana.cern.ch/d/EkryqaV7z/leaf-switch-sw-s3r11-b1?orgId=1&from=1630674878308&to=1630677122980)


## Requirements

* python3
* prometheus-client (needs to be installed with pip)
* `ibqueryerrors` and `perfquery`: clone and compile [this RDMA-core project](https://gitlab.cern.ch/magomis/rdma-core-lhcb-eb-daq)

## Usage
Metrics are exported on the chosen HTTP port, events like counter reset will be on STDOUT. 

```
usage: infiniband-exporter.py [-h] [--port PORT] [--can-reset-counter]
                              [--node-name-map NODE_NAME_MAP]
                              [--verbose]

Prometheus collector for a infiniband fabric

optional arguments:
  -h, --help            show this help message and exit
  --port PORT           Collector http port, default is 9683
  --can-reset-counter   Will reset counter as required when maxed out. Can
                        also be set with env variable CAN_RESET_COUNTER.
                        It should not be needed thanks to the extended versions
                        (64-bit) of the rapidly growing counters. Faulse by default.
  --node-name-map NODE_NAME_MAP
                        Node name map used by ibqueryerrors. Can also be set
                        with env var NODE_NAME_MAP. By default: /admin/etc/ib-node-name-map
                        for the LHCb DAQ network.
  --verbose             increase output verbosity
```
## Daemon configuration
When using the RPM, some parameters can be set in a file so systemd will pass them to the daemon (`infiniband-exporter`).

```
cat /etc/sysconfig/infiniband-exporter.conf
NODE_NAME_MAP=/etc/node-name-map
CAN_RESET_COUNTER=TRUE
```

## Labels

4 labels according to SNMP syntax, as well as their counterparts corresponding to the remode node:

| Label name                       | Description                                                                |
| -------------------------------- | -------------------------------------------------------------------------- |
| ifAlias                          | (Local) node GUID                                                          |
| ifDescr                          | (Local) switch / HCA name                                                  |
| ifName                           | (Local) node LID                                                           |
| ifIndex                          | (Local) port number                                                        |
| ifAliasRemote                    | Remote node GUID                                                           |
| ifDescrRemote                    | Remote switch / HCA name                                                   |
| ifNameRemote                     | Remote node LID                                                            |
| ifIndexRemote                    | Remote port number                                                         |

## Metrics

When exported, the suffix "\_total" is automatically appended to all conters in order to distinguish them from gauges, following the Prometheus convention (see the file prometheus_client/metrics.py in [Prometheus client Python](https://github.com/prometheus/client_python)).  

### Global Gauges

| Name                                             | Description                                                                |
| ------------------------------------------------ | -------------------------------------------------------------------------- |
| scrape\_ok                                       | Indicates with a 1 if the scrape was successful and complete, otherwise 0. |
| scrape\_duration\_seconds                        | Number of seconds taken to collect and parse the stats.                    |
| ibqueryerrors\_duration\_seconds                 | Number of seconds taken to run ibqueryerrors (only errors).                |
| ibqueryerrors\_only\_counters\_duration\_seconds | Number of seconds taken to run ibqueryerrors (only performance).           |


### Error Gauges from STDERR by ibqueryerrors

| Name                    | Labels                                   | Description                                                         |
| ----------------------- | ---------------------------------------- | ------------------------------------------------------------------- |
| bad\_status\_error      | path, status, error                      | Bad status error catched from STDERR by ibqueryerrors.              |
| query\_failed\_error    | counter\_name, local\_name, lid, port    | Failed query catched from STDERR by ibqueryerrors.                  |
| mad\_rpc\_failed\_error | portid                                   | ibwarn\_mad\_rpc error catched from STDERR by ibqueryerrors.        |
| query\_cap\_mask\_error | counter\_name, local\_name, portid, port | bwarn\_query\_cap\_mask error catched from STDERR by ibqueryerrors. |
| print\_error            | counter\_name, local\_name, portid, port | ibwarn\_print\_error catched from STDERR by ibqueryerrors.          |

### Channel Adapters (CA) and Switches Metrics

InfiniBand metrics are translated to SNMP according to the table included in: mapping_table/. 

#### Performance Counters

| Name                  | Description                                                                                                                                                                            |
| --------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ifHCOutOctets         | Total number of data octets transmitted on each VL, between (and not including) the start of packet delimiter and the VCRC (Variant Cyclic Redundancy Check); including packets containing errors and excluding link packets. Results reported as a multiple of 4 octets.|
| ifHCInOctets          | Total number of data octets received on each VL, between (and not including) the start of packet delimiter and the VCRC; including packets containing errors and excluding link packets. Results reported as a multiple of 4 octets. |
| ifHCOutUcastPkts      | Total number of unicast packets transmitted on all VLs, including unicast packets containing errors and excluding link packets.                                                                    |
| ifHCInUcastPkts       | Total number of unicast packets received on all VL, including unicast packets containing errors and excluding link packets.                                                                                                          |
| ifHCOutMulticastPkts  | Total number of multicast packets transmitted on all VLs, including multicast packets containing errors.                                                                |
| ifHCInMulticastPkts   | Total number of multicast packets received on all VLs, including multicast packets containing errors.                                                                                                      |
| *ibIfOutWait*         | The number of ticks during which the port had data to transmit but no data was sent during the entire tick (either because of insufficient credits or because of lack of arbitration). |

#### Error Counters

##### Errors in transmitted packets

| Name                            | Description                                                                                                      |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| **ifOutDiscards**               | Total number of outbound packets discarded by the port because the port is down or congested.                    |
| *ibIfInactiveDiscards*          | Total number of packets discarded due to the output port being in the inactive state.                            |
| *ibIfNeighborMTUDiscards*       | Total outbound packets discarded by the port because packet length exceeded the neighbor MTU.                    |
| *ibIfSwLifetimeLimitDiscards*   | Total number of outbound packets discarded by the portbecause the switch lifetime limit was exceeded.            |
| *ibIfSwHOQLifetimeLimitDiscards*| Total number of outbound packets discarded by the portbecause the HoQ (Head of Queue: length of time a packet can remain at the head of a VL queue) lifetime limit of the switch containing the port was exceeded.                                                     |

##### Errors in received packets

| Name                            | Description                                                                                                      |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| **ifInErrors**                  | Total number of packets containing errors that were received on the port.                                        |
| *ibIfLocalPhysicalErrors*       | Total number of received packets containing a physical error such as CRC (Cyclic Redundancy Check) error -ICRC, VCRC, LPCRC- that cause entry into the BAD PACKET or BAD PACKET discard states of the packet receiver state machine.        |
| *ibIfMalformedPktErrors*        | Total number of received packets on the port containing malformed data or link packet errors (LVer, length, VL). |
| *ibIfBufferOverrunErrors*       | Total number of received packets on the part discarded due to buffer overrrun.                                   |
| *ibIfDLIDMappingErrors*         | Total number of packets on the port that could not be forwarded by the switch due to "Destination LID" (DLID) mapping errors.              |
| *ibIfVLMappingErrors*           | Total number of packets on the port that could not be forwared by the switch due to Virtual Lane (VL) mapping errors.                      | 
| *ibIfLoopingErrors*             | Total number of packets on the port that could not be forwarded by the switch due to looping errors (otuput port = input port).               |

##### Other errors
| Name                                | Description                                                                                                  |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| *ibIfSymbolErrors*                  | Total number of minor link errors detected on one or more physical lanes.                                    |
| *ibIfLinkErrorRecovery*             | Total number of times the Port Training state machine has successfully completed the link error recovery process.                         | 
| *ibIfLinkDowned*                    | Total number of times the Port Training state machine has failed the link error recovery process and downed the link.                        |
| *ibIfRcvRemotePhysicalErrors*       | Total number of packets marked with the EBP delimiter received on the port.                                  |
| *ibIfOutConstraintErrors*           | Total number of packets not transmitted from the switch physical port for the following reasons:<br>- FilterRawOutbound is true and packet is raw<br>- PartitionEnforcementOutbound is true and packet fails partition key check or IP version check                            |
| *ibIfInConstraintErrors*            | Total number of packets received on the switch physical port that are discarded for the following reasons:<br>- FilterRawInbound is true and packet is raw<br>- PartitionEnforcementInbound is true and packet fails partition key check or IP version check                            |
| *ibIfLocalLinkIntegrityErrors*      | The number of times that the count of local physical errors exceeded the threshold specified by LocalPhyErrors.                  |
| *ibIfExcessiveBufferOverrunErrors*  | The number of times that OverrunErrors consecutive flow control update periods occurred, each having at least one overrun error.               |
| *ibIfQP1Dropped*                    | The number of GSI (General Services Interface) packets dropped. Unlike the other Queue Pairs (QP), which are created and destroyed on an as-needed basis, QP1 is permanently implemented. Unlike the subnet management packets transferred on VL15, GSI packets don't require high priority.     |
| *ibIfVL15Dropped*                   | The number of incoming VL15 packets dropped due to resource limitations (for example, lack of buffers) in the port.                            |

#### Informative Gauges

| Name            | Description                                                                                                              |
| --------------- | ------------------------------------------------------------------------------------------------------------------------ |
| linkSpeedActive | Link current speed per lane.                                                                                             |
| linkWidthActive | Number of lanes per link.                                                                                                |
| ifAdminStatus   | Desired (logical) link state. 1: "Active", 2: "Down", 3: "Initializing" **to be implemented** (4: "Armed" and 5: "Unknown" not implemented either).                                 |
| ifOperStatus    | Current operational (physical) link state. 1: "LinkUp", 2: "Disabled", 3: "Polling", (4: "Sleep" and 5: "PortConfigurationTraining" not implemented).        |

### Example

```
# HELP ifHCOutOctets_total Total number of data octets transmitted on each VL. Multiply by 4 to obtain the data octets transmitted on the whole link.
# TYPE ifHCOutOctets_total counter
ifHCOutOctets_total{ifAlias="0xc42a103008822bc",ifAliasRemote="0x0c42a103002b6b56",ifDescr="mlx5_0",ifDescrRemote="SW-S4R17-B1",ifIndex="1",ifIndexRemote="16",ifName="271",ifNameRemote="110"} 3.420000936461e+013
ifHCOutOctets_total{ifAlias="0xc42a1030088238c",ifAliasRemote="0x0c42a103002b6b56",ifDescr="mlx5_1",ifDescrRemote="SW-S4R17-B1",ifIndex="1",ifIndexRemote="15",ifName="191",ifNameRemote="110"} 1.9145719254327e+013
[...]
# HELP ifOutDiscards_total Total number of outbound packets discarded by the port because the port is down or congested.
# TYPE ifOutDiscards_total counter
ifOutDiscards_total{ifAlias="0xc42a10300896530",ifAliasRemote="0x0c42a103001b4aa6",ifDescr="mlx5_1",ifDescrRemote="SW-S4R11-B1",ifIndex="1",ifIndexRemote="3",ifName="56",ifNameRemote="232"} 1.0
ifOutDiscards_total{ifAlias="0xc42a10300257c66",ifAliasRemote="0x0c42a103008820d8",ifDescr="SW-S3R14-B1",ifDescrRemote="saeb17 mlx5_1",ifIndex="15",ifIndexRemote="1",ifName="130",ifNameRemote="324"} 4.0
[...]
# HELP ifAdminStatus Desired (logical) link state. 1: "Active", 2: "Down"
# TYPE ifAdminStatus gauge
ifAdminStatus{ifAlias="0xc42a103002b6b56",ifAliasRemote="0x0c42a103007ca0a8",ifDescr="SW-S4R17-B1",ifDescrRemote="hceb04 mlx5_1",ifIndex="7",ifIndexRemote="1",ifName="110",ifNameRemote="236"} 1.0
ifAdminStatus{ifAlias="0xc42a103002b6b56",ifAliasRemote="0x0c42a103007ca0b8",ifDescr="SW-S4R17-B1",ifDescrRemote="hceb04 mlx5_0",ifIndex="8",ifIndexRemote="1",ifName="110",ifNameRemote="269"} 1.0
ifAdminStatus{ifAlias="0xc42a103002b6b56",ifDescr="SW-S4R17-B1",ifIndex="9",ifName="110"} 2.0
```
